import sqlite3
import csv
from sqlite3 import Error
 
def sql_connection():
    try:
        con = sqlite3.connect('mydatabase.db')
        return con
    except Error:
        print(Error)
 
def sql_table(con):
    cursorObj = con.cursor()
    cursorObj.execute("CREATE TABLE IF NOT EXISTS voiture(addresse_titulaire text,nom text,prenom text,immatriculation text PRIMARY KEY,date_immatriculation text,vin text,marque text,denomination_commerciale text,couleur text,carroserie text,categorie text,cylindree text,energie text,places text,poids text,puissance text,type text,variante text,version text)")
    con.commit()
 
def sql_insert(con, entities):
    cursorObj = con.cursor()
    cursorObj.execute('INSERT INTO voiture(addresse_titulaire,nom,prenom,immatriculation,date_immatriculation,vin,marque,denomination_commerciale,couleur,carroserie,categorie,cylindree,energie,places,poids,puissance,type,variante,version) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', entities)
    con.commit()          


def sql_update(con):
    cursorObj = con.cursor()
    #SELECT COUNT(immatriculation) from voiture where immatriculation=new_immatriculation
    cursorObj.execute('UPDATE voiture SET nom=?, prenom=?, adresse_titulaire=?, where immatriculation = 2')
    con.commit()


def sql_fetch(con): 
    cursorObj = con.cursor()
    cursorObj.execute('SELECT * FROM voiture')
    rows = cursorObj.fetchall()
    print(rows)


def upsert(ligne):
    entities = ligne
    con = sqlite3.connect('mydatabase.db')
    sql_insert(con,entities)


def lire_fichier_csv():
    with open('New_votre_fichier.csv') as csvfile:
        reader=csv.reader(csvfile, delimiter=',')
        for ligne in reader:
            upsert(ligne)





con = sql_connection()
sql_table(con)
lire_fichier_csv()
#sql_update(con)
sql_fetch(con)
